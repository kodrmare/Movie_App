package com.example.marek.movie_app;

import android.app.Application;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.ViewHolder>
{

	private final List<String> trailer_images;
	private OnClickListener mListener;

	public interface OnClickListener{
		void onClick(String data);
	}

	public TrailerAdapter(List<String> actors_images, OnClickListener listener) {
		this.trailer_images = actors_images;
		mListener = listener;

	}



	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		LayoutInflater li = LayoutInflater.from(parent.getContext());
		return new ViewHolder(li.inflate(R.layout.video_item, parent, false), mListener);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		holder.bind(trailer_images.get(position));
	}


	@Override
	public int getItemCount()
	{
		return trailer_images.size();
	}


	class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{

		ImageView mImage;
		OnClickListener mListener;

		public ViewHolder(View itemView, OnClickListener listener)
		{
			super(itemView);
			itemView.setOnClickListener(this);
			mListener = listener;

			//find ImageView
			mImage = (ImageView) itemView.findViewById(R.id.imageview_trailer);
		}

		public void bind(String actors_image)
		{
			//set value to ImageView
			Glide.with(mImage.getContext()).load(actors_image).into(mImage);
		}


		@Override
		public void onClick(View v)
		{
			mListener.onClick(trailer_images.get(getAdapterPosition()));
		}
	}
}

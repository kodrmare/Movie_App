package com.example.marek.movie_app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


public class ActorsFragment extends Fragment
{
	private String[] mImages;
	private String[] mNames;
	private String[] mDescription;
	private String[] mBirth;
	private String[] mMovies;
	private selectedFragment mListener;


	public interface selectedFragment
	{
		void onMovieSelected(String imageResId, String name, String description, String year, String director);
	}


	public ActorsFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_actors, container, false);

		GridView gridView = (GridView) rootView.findViewById(R.id.gridview);
		final ActorAdapter actorsAdapter = new ActorAdapter(getContext(), mNames, mImages);
		gridView.setAdapter(actorsAdapter);

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String name = mNames[position];
				String image = mImages[position];
				String movies = mMovies[position];
				String birth = mBirth[position];
				String description = mDescription[position];

				Intent nextScreen = new Intent(getContext(), ActorDetailActivity.class);

				nextScreen.putExtra("imageResId", image);
				nextScreen.putExtra("name", name);
				nextScreen.putExtra("birth", birth);
				nextScreen.putExtra("movies", movies);
				nextScreen.putExtra("description", description);

				startActivity(nextScreen);
			}
		});

		// Inflate the layout for this fragment
		return rootView;
	}


	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);

		final Resources resources = context.getResources();
		mNames = resources.getStringArray(R.array.actors);
		mImages = resources.getStringArray(R.array.actor_images);
		mDescription = resources.getStringArray(R.array.actor_description);
		mBirth = resources.getStringArray(R.array.day_of_birth);
		mMovies = resources.getStringArray(R.array.number_of_movies);

	}


	@Override
	public void onDetach() {
		super.onDetach();
	}




}

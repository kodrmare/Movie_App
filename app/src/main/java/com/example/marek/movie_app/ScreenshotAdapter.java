package com.example.marek.movie_app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by marek on 13/08/2017.
 */

public class ScreenshotAdapter extends RecyclerView.Adapter<ScreenshotAdapter.ViewHolder>
{

	private final List<String> screenshot_images;

	public ScreenshotAdapter(List<String> screenshot_images) {
		this.screenshot_images = screenshot_images;
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		LayoutInflater li = LayoutInflater.from(parent.getContext());
		return new ViewHolder(li.inflate(R.layout.screenshot_item, parent, false));
	}


	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		holder.bind(screenshot_images.get(position));
	}


	@Override
	public int getItemCount()
	{
		return screenshot_images.size();
	}


	class ViewHolder extends RecyclerView.ViewHolder
	{

		ImageView mImage;

		public ViewHolder(View itemView)
		{
			super(itemView);

			//find ImageView
			mImage = (ImageView) itemView.findViewById(R.id.imageview_screenshot);

		}


		public void bind(String actors_image)
		{
			//set value to ImageView
			Glide.with(mImage.getContext()).load(actors_image).into(mImage);
		}
	}
}

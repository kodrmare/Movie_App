package com.example.marek.movie_app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Arrays;
import java.util.List;


public class ProfileFragment extends Fragment
{
	public ProfileFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

		// set circle mask for actors image
		ImageView imageView2 = (ImageView) rootView.findViewById(R.id.profile_image);
		Bitmap avatar = BitmapFactory.decodeResource(getResources(), R.drawable.hardy);
		RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), avatar);
		roundDrawable.setCircular(true);
		imageView2.setImageDrawable(roundDrawable);


		RecyclerView list = (RecyclerView) rootView.findViewById(R.id.list_profile);
		list.setLayoutManager(new GridLayoutManager(getContext(), 3));

		//get array of movies from DB
		String[] array = getResources().getStringArray(R.array.actor_movies);
		List<String> movieList = Arrays.asList(array);

		list.setAdapter(new ActorDetailMovieListAdapter(movieList.subList(0, 6)));


		return rootView;
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}



}

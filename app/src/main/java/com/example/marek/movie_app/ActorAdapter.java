package com.example.marek.movie_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;



public class ActorAdapter extends BaseAdapter
{
	private final Context mContext;
	private final String[] actors;
	private final String[] actors_images;

	public ActorAdapter(Context context, String[] actors, String[] actors_images) {
		this.mContext = context;
		this.actors = actors;
		this.actors_images = actors_images;
	}

	@Override
	public int getCount()
	{
		return actors.length;
	}


	@Override
	public long getItemId(int position)
	{
		return 0;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		final String name = actors[position];
		final String image = actors_images[position];

		if (convertView == null)
		{
			final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
			convertView = layoutInflater.inflate(R.layout.actor_recycle_item, null);

			final ImageView imageViewItem = (ImageView)convertView.findViewById(R.id.imageview_actor_name);
			final TextView textViewItem = (TextView)convertView.findViewById(R.id.textview_actor_name);

			final ViewHolder viewHolder = new ViewHolder(textViewItem, imageViewItem);
			convertView.setTag(viewHolder);
		}

		final ImageView imageViewItem = (ImageView)convertView.findViewById(R.id.imageview_actor_name);

		final ViewHolder viewHolder = (ViewHolder)convertView.getTag();
		viewHolder.nameTextView.setText(name);
		Glide.with(mContext).load(image).into(imageViewItem);

		return convertView;
	}

	@Override
	public Object getItem(int position)
	{
		return null;
	}

	private class ViewHolder {
		private final TextView nameTextView;
		private final ImageView autorImageView;

		public ViewHolder(TextView nameTextView, ImageView autorImageView) {
			this.nameTextView = nameTextView;
			this.autorImageView = autorImageView;
		}
	}
}

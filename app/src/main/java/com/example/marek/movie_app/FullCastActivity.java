package com.example.marek.movie_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;


public class FullCastActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_cast);

		Intent i = getIntent();
		String imageResId = i.getStringExtra("imageResId");  //for later use
		String movieName = i.getStringExtra("movieName");


		TextView title = (TextView) findViewById(R.id.full_cast_title);

		title.setText(movieName);

		// back arrow implement
		ImageView arrowBack = (ImageView) findViewById(R.id.detail_arrow_back);
		arrowBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		RecyclerView listCast = (RecyclerView) findViewById(R.id.full_cast_list_detail_actors);

		listCast.setLayoutManager(new LinearLayoutManager(this));

		String[] arrayCastName = getResources().getStringArray(R.array.actors);
		String[] arrayCast = getResources().getStringArray(R.array.actor_images);
		String[] arrayCastRole = getResources().getStringArray(R.array.roles);


		List<String> movieListCast = Arrays.asList(arrayCast);
		List<String> movieListCastName = Arrays.asList(arrayCastName);
		List<String> movieListCastRole = Arrays.asList(arrayCastRole);



		listCast.setAdapter(new CastAdapter(movieListCast, movieListCastName, movieListCastRole, new CastAdapter.OnClickListener()
		{
			@Override
			public void onClick(String data)
			{
				Toast.makeText(FullCastActivity.this, data, Toast.LENGTH_SHORT).show();
			}
		}));




	}
}

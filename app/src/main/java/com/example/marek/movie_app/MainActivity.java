package com.example.marek.movie_app;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements ListFragment.selectedFragment
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		setTitle("Movies");

		BottomNavigationView mBottomNav = (BottomNavigationView) findViewById(R.id.navigation);

		if(savedInstanceState == null)
		{
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.root_layout, ListFragment.newInstance(), "movieList")
					.commit();
		}

		//handling bottomNavigationBar
		mBottomNav.setOnNavigationItemSelectedListener(

				new BottomNavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(@NonNull MenuItem item) {
						Fragment fragment = null;

						if(item.getTitle().equals("Movies") ){
							setTitle("Movies");
							fragment = new ListFragment();
						}
						else if(item.getTitle().equals("Actors")){
							setTitle("Actors");
							fragment = new ActorsFragment();
						}
						else if(item.getTitle().equals("Profile")){
							setTitle("");
							fragment = new ProfileFragment();
						}
						FragmentManager fragmentManager = getSupportFragmentManager();
						android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						fragmentTransaction.replace(R.id.root_layout, fragment);
						fragmentTransaction.commit();
						return true;
					}
				});

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	//search button clicked
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if(id == R.id.action_search){
			Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMovieSelected(String imageResId, String name, String description, String year, String director)
	{

		Intent nextScreen = new Intent(getApplicationContext(), DetailActivity.class);

		nextScreen.putExtra("imageResId", imageResId);
		nextScreen.putExtra("name", name);
		nextScreen.putExtra("description", description);
		nextScreen.putExtra("year", year);
		nextScreen.putExtra("director", director);


		startActivity(nextScreen);


	}
}

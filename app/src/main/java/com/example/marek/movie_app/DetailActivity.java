package com.example.marek.movie_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;


public class DetailActivity extends AppCompatActivity
{
	Boolean flag = true;
	String mName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_detail);

		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_detail);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		this.setTitle("");

		Intent i = getIntent();
		String imageResId = i.getStringExtra("imageResId");
		String name = i.getStringExtra("name");
		String description = i.getStringExtra("description");
		String year = i.getStringExtra("year");
		String director = i.getStringExtra("director");

		mName = name;


		final ImageView imageView = (ImageView) findViewById(R.id.comic_image);
		final TextView nameTextView = (TextView) findViewById(R.id.name);
		final TextView descriptionTextView = (TextView) findViewById(R.id.description);
		final TextView directorTextView = (TextView) findViewById(R.id.director);


		Glide.with(this).load(imageResId).into(imageView);
		nameTextView.setText(name);
		directorTextView.setText(director+", " + year);
		descriptionTextView.setText(description);

		// back arrow implement
		ImageView arrowBack = (ImageView) findViewById(R.id.detail_arrow_back);
		arrowBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});


		//..........
		RecyclerView list = (RecyclerView) findViewById(R.id.list_detail_galery);
		RecyclerView listCast = (RecyclerView) findViewById(R.id.list_detail_actors);
		RecyclerView listTrailer = (RecyclerView) findViewById(R.id.list_detail_trailers);


		list.setLayoutManager(new GridLayoutManager(this, 3));
		listCast.setLayoutManager(new LinearLayoutManager(this));
		listTrailer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


		//get array of movies from DB
		String[] array = getResources().getStringArray(R.array.actor_movies);
		String[] arrayCast = getResources().getStringArray(R.array.actor_images);
		String[] arrayTrailer = getResources().getStringArray(R.array.images);

		String[] arrayCastName = getResources().getStringArray(R.array.actors);
		String[] arrayCastRole = getResources().getStringArray(R.array.roles);




		List<String> movieList = Arrays.asList(array);
		List<String> movieListCast = Arrays.asList(arrayCast);
		List<String> movieListTrailer = Arrays.asList(arrayTrailer);

		List<String> movieListCastName = Arrays.asList(arrayCastName);
		List<String> movieListCastRole = Arrays.asList(arrayCastRole);




		list.setAdapter(new ScreenshotAdapter(movieList.subList(0, 6)));

		listCast.setAdapter(new CastAdapter(movieListCast.subList(0, 4), movieListCastName, movieListCastRole, new CastAdapter.OnClickListener()
		{
			@Override
			public void onClick(String data)
			{
				Toast.makeText(DetailActivity.this, data, Toast.LENGTH_SHORT).show();
			}
		}));

		listTrailer.setAdapter(new TrailerAdapter(movieListTrailer, new TrailerAdapter.OnClickListener(

		)
		{
			@Override
			public void onClick(String data)
			{
				Toast.makeText(DetailActivity.this, data, Toast.LENGTH_SHORT).show();
			}
		}));



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_detail, menu);
		return true;
	}



	//search button clicked
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if(id == R.id.action_share){
			Toast.makeText(getApplicationContext(), "Share action is selected!", Toast.LENGTH_SHORT).show();
			return true;
		}
		if(id == R.id.action_favorit){

			if(flag){
				item.setIcon(R.drawable.ic_favorite_black_24dp);
				showToast("Movie added to favourites");
				flag = false;
			}
			else
			{
				item.setIcon(R.drawable.ic_favorite_border_black_24dp);
				showToast("Movie removed from favourites");
				flag = true;
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void showToast(String textToast){
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast,
				(ViewGroup) findViewById(R.id.custom_toast_container));

		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(textToast);

		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(Gravity.BOTTOM, 0, 150);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}

	public void seeAllFavourites(View view){

		Intent nextScreen = new Intent(getApplicationContext(), FullCastActivity.class);

		String id = "id";

		nextScreen.putExtra("movieID", id);
		nextScreen.putExtra("movieName", mName);



		startActivity(nextScreen);
	}
}

package com.example.marek.movie_app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;


public class ListFragment extends Fragment
{
	private String[] mImageResIds;
	private String[] mNames;
	private String[] mDescriptions;
	private String[] mYears;
	private selectedFragment mListener;
	private String[] mDirectors;


	public interface selectedFragment
	{
		void onMovieSelected(String imageResId, String name, String description, String year, String director);
	}


	public static ListFragment newInstance()
	{
		return new ListFragment();
	}


	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);

		if(context instanceof selectedFragment)
		{
			mListener = (selectedFragment) context;
		}
		else
		{
			throw new ClassCastException(context.toString());
		}

		// Get rage face names and descriptions.
		final Resources resources = context.getResources();
		mNames = resources.getStringArray(R.array.names);
		mDescriptions = resources.getStringArray(R.array.descriptions);
		mYears = resources.getStringArray(R.array.years);
		mDirectors = resources.getStringArray(R.array.director);
		mImageResIds = resources.getStringArray(R.array.images);
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		final View view = inflater.inflate(R.layout.fragment_list, container, false);

		final Activity activity = getActivity();
		final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
		//recyclerView.setLayoutManager(new GridLayoutManager(activity, 1));
		recyclerView.setLayoutManager(new LinearLayoutManager(activity));
		recyclerView.setAdapter(new MovieAdapter(activity));
		return view;
	}


	class MovieAdapter extends RecyclerView.Adapter<ViewHolder>
	{

		private LayoutInflater mLayoutInflater;


		public MovieAdapter(Context context)
		{
			mLayoutInflater = LayoutInflater.from(context);
		}


		@Override
		public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
		{
			return new ViewHolder(mLayoutInflater.inflate(R.layout.fragment_recycle_item, viewGroup, false));
		}


		@Override
		public void onBindViewHolder(ViewHolder viewHolder, final int position)
		{
			final String imageResId = mImageResIds[position];
			final String name = mNames[position];
			final String description = mDescriptions[position];
			final String year = mYears[position];
			final String director = mDirectors[position];

			viewHolder.setData(imageResId, name, description, getContext());

			viewHolder.itemView.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					mListener.onMovieSelected(imageResId, name, description, year, director);
				}
			});
		}


		@Override
		public int getItemCount()
		{
			return mNames.length;
		}
	}

	class ViewHolder extends RecyclerView.ViewHolder
	{
		// Views
		private ImageView mImageView;
		private TextView mNameTextView;
		private TextView mContentView;


		private ViewHolder(View itemView)
		{
			super(itemView);

			// Get references to image and name.
			mImageView = (ImageView) itemView.findViewById(R.id.comic_image);
			mNameTextView = (TextView) itemView.findViewById(R.id.name);
			mContentView = (TextView) itemView.findViewById(R.id.contentText);
		}


		private void setData(String imageResId, String name, String contentText, Context context)
		{
			Glide.with(context).load(imageResId).into(mImageView);
			mNameTextView.setText(name);
			mContentView.setText(contentText);
		}
	}
}

package com.example.marek.movie_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;


public class ActorDetailActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_actor_detail);

		//set data to current layout

		Intent i = getIntent();
		String image = i.getStringExtra("imageResId");
		String name = i.getStringExtra("name");
		String movies = i.getStringExtra("movies");
		String birth = i.getStringExtra("birth");
		String description = i.getStringExtra("description");

		TextView mName = (TextView)findViewById(R.id.actor_detail_name);
		TextView mDescription = (TextView) findViewById(R.id.actor_detail_description);
		ImageView mImage = (ImageView) findViewById(R.id.actor_detail_image);
		TextView mBirth = (TextView) findViewById(R.id.actor_detail_birth);
		TextView mMovies = (TextView) findViewById(R.id.actor_detail_movies);

		mBirth.setText(birth);
		mMovies.setText(movies);
		mName.setText(name);
		mDescription.setText(description);
		Glide.with(this).load(image).into(mImage);

		// back arrow implement
		ImageView arrowBack = (ImageView) findViewById(R.id.actor_detail_arrow_back);
		arrowBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});


		//Recycle implementation
		RecyclerView list = (RecyclerView) findViewById(R.id.list);
		list.setLayoutManager(new GridLayoutManager(this, 3));

		//get array of movies from DB
		String[] array = getResources().getStringArray(R.array.actor_movies);
		List<String> movieList = Arrays.asList(array);

		list.setAdapter(new ActorDetailMovieListAdapter(movieList.subList(0, 6)));

	}

}

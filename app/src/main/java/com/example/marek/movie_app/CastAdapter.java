package com.example.marek.movie_app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by marek on 13/08/2017.
 */

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.ViewHolder>
{

	private final List<String> actors_images;
	private final List<String> actors_names;
	private final List<String> actors_role;

	private OnClickListener mListener;

	public interface OnClickListener{
		void onClick(String data);
	}



	public CastAdapter(List<String> actors_images, List<String> actors_names, List<String> actors_role, OnClickListener listener) {

		this.actors_images = actors_images;
		this.actors_names = actors_names;
		this.actors_role = actors_role;
		this.mListener = listener;


	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		LayoutInflater li = LayoutInflater.from(parent.getContext());
		return new ViewHolder(li.inflate(R.layout.layout_cast_recycle_item, parent, false), mListener);

	}


	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		holder.bind(actors_images.get(position), actors_names.get(position), actors_role.get(position));
	}


	@Override
	public int getItemCount()
	{
		return actors_images.size();
	}


	class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{

		ImageView mImage;
		TextView mName;
		TextView mRole;
		OnClickListener mListener;



		public ViewHolder(View itemView, OnClickListener listener)
		{
			super(itemView);

			itemView.setOnClickListener(this);
			mListener = listener;

			//find ImageView
			mImage = (ImageView) itemView.findViewById(R.id.cast_image);
			mName = (TextView) itemView.findViewById(R.id.cast_name);
			mRole = (TextView) itemView.findViewById(R.id.cast_role);

		}


		public void bind(String actors_image, String actors_name, String actors_role)
		{
			//set value to ImageView
			Glide.with(mImage.getContext()).load(actors_image).into(mImage);
			mName.setText(actors_name);
			mRole.setText(actors_role);

		}


		@Override
		public void onClick(View v)
		{
			mListener.onClick(actors_names.get(getAdapterPosition()));
		}
	}

}

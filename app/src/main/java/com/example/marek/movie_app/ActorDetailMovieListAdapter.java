package com.example.marek.movie_app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by marek on 08/08/2017.
 */

public class ActorDetailMovieListAdapter extends RecyclerView.Adapter<ActorDetailMovieListAdapter.ViewHolder>
{
	private final List<String> actors_images;

	//constructor
	public ActorDetailMovieListAdapter(List<String> actors_images) {
		this.actors_images = actors_images;
	}


	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		LayoutInflater li = LayoutInflater.from(parent.getContext());
		return new ViewHolder(li.inflate(R.layout.actor_movie_recycle_item, parent, false));
	}


	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		holder.bind(actors_images.get(position));
	}

	@Override
	public int getItemCount()
	{
		//count of items
		return actors_images.size();
	}


	class ViewHolder extends RecyclerView.ViewHolder
	{

		ImageView mImage;

		public ViewHolder(View itemView)
		{
			super(itemView);

			//find ImageView
			mImage = (ImageView) itemView.findViewById(R.id.imageview_actor_name);

		}


		public void bind(String actors_image)
		{
			//set value to ImageView
			Glide.with(mImage.getContext()).load(actors_image).into(mImage);
		}
	}

}
